from django.apps import AppConfig


class SettingsUiConfig(AppConfig):
    name = 'django_ui'
